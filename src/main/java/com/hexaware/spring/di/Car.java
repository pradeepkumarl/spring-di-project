package com.hexaware.spring.di;

import org.springframework.stereotype.Component;

public interface Car {
 void commute(String source, String destination);

 void setPricePerKm(double pricePerKm);

 double getPricePerKm();
}