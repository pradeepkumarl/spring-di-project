package com.hexaware.spring.di;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class SavingsAccount extends BankAccount {

    public SavingsAccount(@Value("525411") long accountId, @Value("Pradeep") String accountName) {
        super(accountId, accountName);
    }

    public void deposit(double amount ){
        super.setAccountBalance(super.getAccountBalance() + amount);
    }

    @PostConstruct
    public void initialize(){
        System.out.println(" Came inside the init method");
    }

    @PreDestroy
    public void tearDown(){
        System.out.println(" Came inside the tear down method");
    }
}