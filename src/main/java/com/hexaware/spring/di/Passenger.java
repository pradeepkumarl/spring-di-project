package com.hexaware.spring.di;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Passenger {

    private Car car;

    private List<Address> addressList;

    public Passenger(Car car){
        this.car = car;
    }

    public void commute(String source, String destination){
        car.commute(source, destination);
    }

    public Car getCar(){
        return this.car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }
}