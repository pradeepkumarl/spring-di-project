package com.hexaware.spring.di;

import org.springframework.stereotype.Component;

@Component
public class MockUber implements Car {
    private double pricePerKm;

    public void commute(String source, String destination) {
        System.out.println(" A Mock implementation to test the behavour");
    }

    public void setPricePerKm(double pricePerKm) {
        this.pricePerKm = pricePerKm;
    }

    public double getPricePerKm() {
        return this.pricePerKm;
    }
}